package com.stamina;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class StaminaApplication {

	public static void main(String[] args) {
		SpringApplication.run(StaminaApplication.class, args);
	}
}
