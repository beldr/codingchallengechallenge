package com.stamina.controller;

import com.stamina.service.RepoDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;


@Controller
public class RepoDownloadController {
    @Autowired
    RepoDownloadService repoDownloadService;

    @PostMapping(value = "/downloadRepo", consumes = "text/plain")
    public ResponseEntity<?> downloadRepo(@Valid @RequestBody String repoUrl) {
        try {
            repoDownloadService.downloadRepoAsZip(repoUrl);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return ResponseEntity.ok(e);
        }
        return ResponseEntity.ok("Nice, URL ok");
    }

    @PostMapping(value = "/downloadRepos", consumes = "text/plain")
    public ResponseEntity<?> downloadRepos(@Valid @RequestBody String repoUrl) {
        List<String> listOfRepos = Arrays.asList(repoUrl.split("\\s+"));
        listOfRepos.forEach(x -> {
            try {
                repoDownloadService.downloadRepoAsZip(x);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        });
        return ResponseEntity.ok("Nice, URL ok");
    }
}
