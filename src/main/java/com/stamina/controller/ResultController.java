package com.stamina.controller;

import com.stamina.exception.ResourceNotFoundException;
import com.stamina.model.Result;
import com.stamina.repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class ResultController {

    @Autowired
    ResultRepository resultRepository;

    @GetMapping("/results")
    public List<Result> getAllResults() {
        return resultRepository.findAll();
    }

    @PostMapping("/results")
    public Result createResult(@Valid @RequestBody Result result) {
        return resultRepository.save(result);
    }

    @GetMapping("/results/{id}")
    public Result getResultById(@PathVariable(value = "id") Long resultId) {
        return resultRepository.findById(resultId)
                .orElseThrow(() -> new ResourceNotFoundException("Result", "id", resultId));
    }

    @PutMapping("/results/{id}")
    public Result updateResult(@PathVariable(value = "id") Long resultId,
                               @Valid @RequestBody Result resultDetails) {

        Result result = resultRepository.findById(resultId)
                .orElseThrow(() -> new ResourceNotFoundException("Result", "id", resultId));

        result.setTitle(resultDetails.getTitle());
        result.setContent(resultDetails.getContent());

        Result updatedResult = resultRepository.save(result);
        return updatedResult;
    }

    @DeleteMapping("/results/{id}")
    public ResponseEntity<?> deleteResult(@PathVariable(value = "id") Long resultId) {
        Result result = resultRepository.findById(resultId)
                .orElseThrow(() -> new ResourceNotFoundException("Result", "id", resultId));

        resultRepository.delete(result);

        return ResponseEntity.ok().build();
    }
}
