package com.stamina.downloadedRepos;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class Compiler {

    public static void main(String[] args) {
        final String PATH = "src/main/java/com/stamina/downloadedRepos/TestAnagram.java";
        try {
            System.out.println(returnedTime(PATH));
        } catch (Exception e) {
            System.out.println(e);
            //return null here or throw error and get null from error elsewhere?
        }
    }

    private static int returnedTime(String PATH) throws Exception {
        String className = PATH.split("/")[PATH.split("/").length - 1].split("\\.")[0];
        Process gen = Runtime.getRuntime().exec("javac -cp src " + PATH); //make into array members if breaks
        gen.waitFor();
        File file = new File("src\\main\\java\\com\\stamina\\downloadedRepos\\" + className + ".class");

        Process pro = Runtime.getRuntime().exec("java -cp src com/stamina/downloadedRepos/" + className + " src/main/java/com/stamina/downloadedRepos/wordlist.txt ram");
        String line;
        int time;

        BufferedReader in1 = new BufferedReader(new InputStreamReader(pro.getErrorStream()));
        while ((line = in1.readLine()) != null) {
            System.out.println(line);
        }

        line = null;
        BufferedReader in = new BufferedReader(new InputStreamReader(pro.getInputStream()));
        if ((line = in.readLine()) != null) {
            time = Integer.parseInt(line.split(",")[0]);
            pro.waitFor();
        }
        else {
            //file.delete();
            throw new RuntimeException("No time returned or bad return format");
        }

        //file.delete();
        return time;
    }

}
