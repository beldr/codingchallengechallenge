package com.stamina.repository;

import com.stamina.model.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {

}
