package com.stamina.service;

import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.UnsupportedAddressTypeException;

@Service
public class RepoDownloadService {

    private static final String GITHUB_APPEND_FOR_ZIP = "/zipball/master";

    public void downloadRepoAsZip(String repoUrl) throws URISyntaxException {

        switch (getDomainName(repoUrl).toUpperCase()) {
            case "GITHUB.COM":
                downloadGitHubRepo(repoUrl);
                break;
            case "BITBUCKET.COM":
                System.err.println("Unsupported repo");
                throw new UnsupportedAddressTypeException();
            case "GITLAB.COM":
                System.err.println("Unsupported repo");
                throw new UnsupportedAddressTypeException();
        }
    }

    private void downloadGitHubRepo(String repoUrl) {
        try (BufferedInputStream in = new BufferedInputStream(new URL(removeTrailingSlashCharactersFromString(repoUrl) + GITHUB_APPEND_FOR_ZIP).openStream());
             FileOutputStream fileOutputStream = new FileOutputStream(( "C:\\Users\\Kauri\\Desktop\\stamina\\GIT\\codingchallengechallenge\\src\\main\\java\\com\\stamina\\downloadedRepos\\temp\\" + System.currentTimeMillis() / 1000L) + "_Downloaded_repo.zip")) {
            System.out.println("Trying to download file");
            byte dataBuffer[] = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }
        } catch (IOException e) {
            // handle exception
            e.printStackTrace();
            System.out.println("File download failed");
        }
    }

    private static String getDomainName(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
        // TODO
    }

    private String removeTrailingSlashCharactersFromString(String unProcessedURL) {
        if (unProcessedURL.endsWith("/")) {
            return unProcessedURL.substring(0, unProcessedURL.length() - 1);
        }
        return unProcessedURL;
    }
}
