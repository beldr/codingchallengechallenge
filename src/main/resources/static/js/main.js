document.getElementById('repo-form').addEventListener('submit', performPostRequest);
function performPostRequest(e) {
    var resultElement = document.getElementById('postResult');
    var repoAddress = document.getElementById('repo-address').value;
    resultElement.innerHTML = '';

    axios.post('http://localhost:8080/downloadRepos', {
        data: repoAddress,
    })
        .then(function (response) {
            resultElement.innerHTML = generateSuccessHTMLOutput(response);
        })
        .catch(function (error) {
            resultElement.innerHTML = generateErrorHTMLOutput(error);
        });

    e.preventDefault();
}
